package pl.edu.pwsztar;

public class Main {

    public static void main( String[] argv ) {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Onion",3,100);
        shoppingCart.addProducts("Carrot",2,100);
        shoppingCart.addProducts("Potatos",3,100);
        shoppingCart.addProducts("Juice",8,100);
        shoppingCart.addProducts("Potatos",3,100);

        System.out.println(shoppingCart.getAllProductsQuantity());
    }
}