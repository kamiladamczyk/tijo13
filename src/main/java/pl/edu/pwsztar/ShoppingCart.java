package pl.edu.pwsztar;

import java.util.*;

public class ShoppingCart implements ShoppingCartOperation {

    Map<String,Product> products = new HashMap<String, Product>();


    public boolean addProducts(String productName, int price, int quantity) {

        if(price <= 0 || quantity <= 0 || productName.equals("")){
            return false;
        }

        int sumOfProducts = getAllProductsQuantity();

        if(sumOfProducts + quantity > PRODUCTS_LIMIT){
            System.out.println("Zbyt wiele produktów w koszyku!");
            return false;
        }
        if(products.containsKey(productName)){
            if(products.get(productName).getPrice() == price){
                Product product = products.get(productName);
                product.addQuantity(quantity);
                products.replace(product.getName(),product);
                return true;
            }else{
                return false;
            }
        }
        products.put(productName, new Product(productName, price, quantity));
        return true;
    }

    public boolean deleteProducts(String productName, int quantity) {
        if(productName.equals("") || quantity <= 0){
            return false;
        }

        Product product = products.get(productName);
        if(Optional.ofNullable(product).isPresent()){
            if(product.getQuantity() > quantity){
                product.delete(quantity);
                return true;
            }else if(product.getQuantity() == quantity){
                products.remove(productName);
                return true;
            }else{
                return false;
            }
        }
        return false;
    }

    public int getQuantityOfProduct(String productName) {
        if(productName.equals("")){
            return 0;
        }
        Product product = products.get(productName);
        if(Optional.ofNullable(product).isPresent()){
            return product.getQuantity();
        }
        return 0;
    }

    public int getAllProductsQuantity(){
        int sumOfProducts = 0;
        for(Map.Entry<String, Product> entry: products.entrySet()){
            sumOfProducts += (entry.getValue().getQuantity());
        }
        System.out.println("Liczba produktów: "+sumOfProducts);
        return sumOfProducts;
    }

    public int getSumProductsPrices() {
        int sum = 0;
        for(Map.Entry<String, Product> entry: products.entrySet()){
            sum += (entry.getValue().getPrice() * entry.getValue().getQuantity());
        }
        return sum;
    }

    public int getProductPrice(String productName) {
        if(productName.equals("")){
            return 0;
        }
        Product product = products.get(productName);
        if(Optional.ofNullable(product).isPresent()){
            return products.get(productName).getPrice();
        }
        return 0;
    }

    public List<String> getProductsNames() {
        List<String> names = new ArrayList<>();
        for(Map.Entry<String, Product> entry :  products.entrySet()){
            names.add(entry.getValue().getName());
        }
        return names;
    }
}