package pl.edu.pwsztar;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

//TODO: Podczas implementacji prosze pamietac o zasadzie F.I.R.S.T
public class AddProductTest {
    ShoppingCart shoppingCart = new ShoppingCart();
    @Test
    void test() {
        Product mock1 = new Product("Milk",2,100);
        Product mock2 = new Product("Butter",2,100);
        Product mock3 = new Product("Eggs",2,100);
        Product mock4 = new Product("Juice",3,100);
        Product mock5 = new Product("Fanta",3,100);
        Product mock6 = new Product("Whisky",100,1);
        Product mock7 = new Product("",0,-1);

        assertTrue(shoppingCart.addProducts(mock1.getName(), mock1.getPrice(), mock1.getQuantity()));
        assertTrue(shoppingCart.addProducts(mock2.getName(), mock2.getPrice(), mock2.getQuantity()));
        assertTrue(shoppingCart.addProducts(mock3.getName(), mock3.getPrice(), mock3.getQuantity()));
        assertTrue(shoppingCart.addProducts(mock4.getName(), mock4.getPrice(), mock4.getQuantity()));
        assertTrue(shoppingCart.addProducts(mock5.getName(), mock5.getPrice(), mock5.getQuantity()));
        assertFalse(shoppingCart.addProducts(mock6.getName(), mock6.getPrice(), mock6.getQuantity()));
        assertFalse(shoppingCart.addProducts(mock7.getName(), mock7.getPrice(), mock7.getQuantity()));

    }
}