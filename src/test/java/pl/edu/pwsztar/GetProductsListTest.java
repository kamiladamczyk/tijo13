package pl.edu.pwsztar;

import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class GetProductsListTest {
    @Test
    void getAllProductsNames(){
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Onion",3,3);
        shoppingCart.addProducts("Carrot",2,5);
        shoppingCart.addProducts("Potatos",3,10);
        shoppingCart.addProducts("Juice",8,1);

        List<String> items = new ArrayList<>();
        items.add("Onion");
        items.add("Carrot");
        items.add("Potatos");
        items.add("Juice");

        assertEquals(items, shoppingCart.getProductsNames());
    }
}