package pl.edu.pwsztar;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ProductPriceTest {

    @Test
    void getProductPrice(){
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Milk",3,2);
        shoppingCart.addProducts("Water",2,20);
        shoppingCart.addProducts("Eggs",6,2);

        assertEquals(3,shoppingCart.getProductPrice("Milk"));
        assertEquals(2,shoppingCart.getProductPrice("Water"));
        assertEquals(6,shoppingCart.getProductPrice("Eggs"));
        assertEquals(0,shoppingCart.getProductPrice("Onion"));
    }
}